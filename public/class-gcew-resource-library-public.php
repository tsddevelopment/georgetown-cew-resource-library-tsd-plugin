<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://topshelfdesign.net
 * @since      1.0.0
 *
 * @package    Gcew_Resource_Library
 * @subpackage Gcew_Resource_Library/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Gcew_Resource_Library
 * @subpackage Gcew_Resource_Library/public
 * @author     Top Shelf Design <info@topshelfdesign.net>
 */
class Gcew_Resource_Library_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     *
     * @param      string $plugin_name The name of the plugin.
     * @param      string $version The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version     = $version;

        add_shortcode("search_test", function(){
            ob_start();
            include(plugin_dir_path( __FILE__) . 'search_test.php');
            return ob_get_clean();
        });
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Gcew_Resource_Library_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Gcew_Resource_Library_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gcew-resource-library-public.css', [], rand(1,99999), 'all' );
        wp_enqueue_style( $this->plugin_name . '-bs-spacing', plugin_dir_url( __FILE__ ) . 'css/bs_spacing.css', [], $this->version, 'all' );
        wp_enqueue_style( 'fancybox', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css', [], $this->version, 'all' );

    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Gcew_Resource_Library_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Gcew_Resource_Library_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_dequeue_script("select2");
        wp_enqueue_script($this->plugin_name . 'select2', plugin_dir_url(__FILE__) . 'js/select2.min.js', ['jquery'], $this->version, true);


        wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gcew-resource-library-public.js', [ 'jquery' ], $this->version, false );

        wp_enqueue_script( 'fancybox', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js', [ 'jquery' ], null, true );
        wp_enqueue_script( 'tsd-script', plugin_dir_url( __FILE__ ) . 'js/bulk-term-editor.js', [ 'jquery' ], null, true );
        wp_localize_script( 'tsd-script', 'tsd', [ 'ajax_url' => admin_url( 'admin-ajax.php' ) ] );

        add_action( 'wp_enqueue_scripts', 'tsd_plugin_scripts' );


    }

}

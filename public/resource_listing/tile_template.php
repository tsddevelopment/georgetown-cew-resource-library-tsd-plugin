<?php

$box_id = uniqid( "tile_" );
$card   = new \TSD\Card( $post );

$title = count( $terms ) ? $terms[0]->name : $post->post_title;

$fallback_icon = "<i class='icon-default-style iconsmind-Newspaper' data-color='extra-color-1'
                   style='font-size: 50px!important; line-height: 50px!important;'>
                </i>";


$pub_date   = date( "F j, Y", strtotime( $post->post_date ) );
$reports    = $card->get_field( "reports" );
$post_link  = $card->get_field( "post_link" );
$embed_link = $card->get_field( "embed_url" );


$link_report_id = false;
if ( $reports && count( $reports ) )
    $link_report_id = $reports[0]->ID;


$media_type = get_the_terms( $post, "media_type" );

if ( $media_type )
    $media_type = $media_type[0];

$acf_id = "{$media_type->taxonomy}_{$media_type->term_id}";
$icon   = get_field( "icon", $acf_id );
$svg    = file_get_contents( $icon['url'] );
$color  = get_field( "color", $acf_id );

$media_type = is_array( $media_type ) ? $media_type->slug : '';

//if ($post->post_type === 'cew_reports')
//    $resources = \TSD\Report::get_resources($post->ID);

$areas = get_the_terms( $post, 'research_area' );

$research_area      = is_array( $areas ) ? $areas[0]->name : '';
$research_area_slug = is_array( $areas ) ? $areas[0]->slug : '';

$report_link = '';

$embed_url = $embed_link ? $embed_link : $post_link;

if ( $card->youtube_url() ):
    $embed_url  = $card->youtube_url();
    $media_type = 'yt_embed';
endif;

if ( $post->post_type === 'cew_reports' ):
    $media_type = 'cew_reports';
    $embed_url  = get_permalink( $post->ID );
endif;

$click = get_field( "click_behavior", $post->ID );

$click_behavior = $click ? "'$click'" : '';

?>

<div class="nectar-fancy-box gcew-resource-tile"
     id="<?= $box_id ?>"
     data-style="color_box_hover"
     data-alignment="center"
     data-color="extra-color-1"
     onclick="(function(e){
                 cew_card_click_fn();
                return false;
            })();return false;"
     data-click-behavior="<?= $click_behavior ?>)"
     data-id="<?= $post->ID ?>"
     data-card-embed-url="<?= $embed_url ?>"
     data-card-media-type="<?= $media_type ?>"
     data-card-post-type="<?= $post->post_type ?>"
     data-card-research-area="<?= $research_area_slug ?>"
>
    <div class="box-inner-wrap">
        <div class="box-bg"></div>
        <div class="inner" style="min-height: 200px">
            <div class="inner-wrap">
                <?= $svg ?>
                <h3 style="font-size: 22px;"><?= $title ?></h3>
            </div>
        </div>
        <a href="#" class="box-link"></a></div>
</div>

<style>

    <?= "#$box_id" ?>
    .box-bg:after {
        background: <?= $color ?> !important;
    }


    <?= "#$box_id" ?>
    svg {
        width: 90px;
        margin-bottom: 15px;
    }

    <?= "#$box_id" ?>
    svg path {
        fill: <?= $color ?> !important
    }

    <?= "#$box_id" ?>:hover
    svg path {

        fill: white !important;
    }

</style>
<?php

$rows    = [];
$csvfile = fopen( "Resource Library Matrix - Keywords.csv", 'rb' );
while ( ! feof( $csvfile ) )
    $rows[] = fgetcsv( $csvfile );


$headers      = array_shift( $rows );
$current_type = '';
array_shift( $rows );
//print_r($rows);

$op = '';
$row_op = '';
$index = 1;
foreach ( $rows as $c => $row ):

    $vals   = [];
    $header = '';
    $values = '';

    if ( $c === 0 ):
        $row_op .= "<tr><td><h2>publications</h2></td><td></td><td></td></tr>";
        $row_op .= "<tr><td><p><strong>Index</strong></p></td><td><p><strong>Title</strong></p></td><td><p><strong>Tags</strong></p></td></tr>";
    else:
        if ( $row[0] !== '' )
            $row_op .= "<tr><td><h2>{$row[0]}</h2></td><td></td><td></td></tr>";
    endif;


    foreach ( $row as $c => $col )
        if ( $col !== '' && $c >= 2):
            $vals[] = $headers[ $c ];
        endif;

    $tags = implode( ", ", $vals );

    $row_op .= "<tr><td>$index</td><td>{$row[1]}</td> <td><p>$tags</p></td></tr>";

    $index++;

endforeach;

$op = "<table class='table'>$row_op</table>";

print $op;

?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<style type="text/css">

    table tr:nth-of-type(odd) {
        background: #fcfcfc;
    }

    p {
        margin: 0;
    }

</style>

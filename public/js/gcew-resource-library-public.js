// global search vars
let page_no = 1,
    action = 'submit',
    max_page_reached = false;

function iframe_modal(url, options = {}) {

    let config = {
        width: 1200,
        height: 650,
        caption: false,
        modal: true
    };

    config = Object.assign(config, options);

    if (url === '' || !url || url === '#') {

        let items = {
            type: 'html',
            opts: {}
        };

        items.src = "<h2>Link Error</h2><hr><p>We're sorry, but there was an issue with the link.</p>";


        items.opts.afterShow = function () {
            jQuery(document).keyup(function (e) {
                // escape key maps to keycode `27`
                if (e.key === "Escape") {
                    jQuery.fancybox.close();
                }
            });

            jQuery("html").css("overflow-y", "hidden");
        };

        items.opts.afterClose = function () {
            jQuery("html").css("overflow-y", "visible");
        };

        items.opts.afterShow = function () {
            jQuery(document).keyup(function (e) {
                // escape key maps to keycode `27`
                if (e.key === "Escape") {
                    jQuery.fancybox.close();
                }
            });

            jQuery("html").css("overflow-y", "hidden");
        };

        items.opts.afterClose = function () {
            jQuery("html").css("overflow-y", "visible");
        };

        jQuery.fancybox.open(items);

    } else {
        let op = '';

        display_url = url;

        if (url.substring(0, 2) === '//')
            display_url = "http:" + url;


        if (url.indexOf('wpengine.netdna') !== -1) {

            // we don't want to show ugly cdn links
            // when normal links work anyways
            display_url = window.origin + display_url.substr(55);

        }


        let button_link = '';

        let embed_url = url;

        if (config.yt_embed) {

            let l = config.yt_embed,
                ls = l.indexOf(".be/"),
                yt_id = l.substr(ls + 4);
            embed_url = "https://www.youtube.com/embed/" + yt_id;
        }


        if (config.show_report_button === true)
            button_link = "<div class='button-link'><a href='" + url + "' target='_blank'>View Report</a></div>";

        op += "<div><div class='top-bar'>" + button_link + "<div class='url' onclick='cew_copy_url(\"" + display_url + "\");'><p>" + display_url + "</p></div></div>";
        op += "<iframe src='" + embed_url + "' width='" + config.width + "' height='" + config.height + "'></iframe></div>";

        let items = {
            src: op,
            type: 'html',
            opts: {}
        };

        if (config.caption)
            items.opts.caption = config.caption;

        //console.log(items);

        items.opts.afterShow = function () {
            jQuery(document).keyup(function (e) {
                // escape key maps to keycode `27`
                if (e.key === "Escape") {
                    jQuery.fancybox.close();
                }
            });

            jQuery("html").css("overflow-y", "hidden");
        };

        items.opts.afterClose = function () {
            jQuery("html").css("overflow-y", "visible");
        };

        console.log(items);

        jQuery.fancybox.open(items);
    }

    return null;
}

function cew_card_click_fn(method = 'default') {

    let t = jQuery(window.event.target),
        obj = t.parents(".cew-resource-card");

    if (jQuery(t).hasClass("cew-resource-card"))
        obj = t;

    if (jQuery(t).hasClass("box-link"))
        obj = t.parents(".gcew-resource-tile");


    let pt = obj.attr("data-card-post-type"),
        mt = obj.attr("data-card-media-type"),
        embed = obj.attr("data-card-embed-url"),
        report_link = obj.attr("data-report-url"),
        post_link = obj.attr("data-link");


    // we're always going to be on https, so
    // we're going to pull off the http portion
    // so the console doesn't squak at us


    if (embed.substring(0, 5) === 'https')
        embed = embed.substring(6);

    if (embed.substring(0, 4) === 'http')
        embed = embed.substring(5);

    // down here, we decide what we do with the card once
    // the user clicks on it.

    let is_report = mt === 'cew_reports',
        is_linkedin = embed.search('linkedin.com') !== -1,
        is_tcf = embed.search('tcf.org') !== -1,
        is_blog = mt === 'blog',
        is_tool = mt === 'interactive-tool',
        is_infographic = mt === 'infographic',
        is_report_pdf = mt === 'report-pdf';

    if (method === 'modal')
        return iframe_modal(embed, {width: 1024});

    if (method === 'popup') {
        let win = window.open(embed, '_blank');
        win.focus();
        return null;
    }

    if (method === 'link') {
        window.location.href = embed;
        return null;
    }


    let site_blocks_iframe = is_linkedin || is_tcf,
        requested_popup = is_report || is_blog || is_tool || is_infographic;


    if (site_blocks_iframe || requested_popup) {
        // if the card is a report, we're just going to send
        // the user off to the new page - no popups here.
        let win = window.open(embed, '_blank');
        win.focus();
        return null;
    }


    if (mt === 'yt_embed') {

        let opts = {};

        opts.yt_embed = embed;

        if (report_link)
            opts.show_report_button = true;

        opts.onActivate = function () {
            alert("test");
        };


        return iframe_modal(embed, opts);
    }

    if (mt === 'newsletter')
        return iframe_modal(embed, {width: 800});

    let opts = {};

    if (report_link)
        opts.show_report_button = true;

    return iframe_modal(embed, opts);

}

function is_cew_home() {
    return jQuery("body").hasClass("page-id-16926");
}

function execute_search(e, form) {


    e.preventDefault();

    if (action == 'submit') {
        jQuery('body').addClass('form-is-loading');
        jQuery(".cew-content").fadeOut();
        jQuery('.spinner').addClass('form-loading').removeClass('hidden');
    } else {
        jQuery('.load-more').removeClass('hidden');
    }

    page_no = (action == 'submit' ? 1 : page_no);

    let form_data = form.find("input, select").serializeArray();
    let content = jQuery(".cew-content");

    form_data.push({
        name: 'page_no',
        value: page_no
    });


    function scan_for_order(data) {

        let r = false;

        jQuery(data).each(function () {
            if (jQuery(this)[0].name === 'cew_order')
                r = true;
        });

        return r;

    }


    if (scan_for_order(form_data)) {

        form_data.push({
            name: 'orderby',
            value: 'date'
        });
        form_data.push({
            name: 'order',
            value: 'asc'
        });
    }


    // so the way this call is configured, we get an array of the posts
    // using the same html template as the get_card_html() function
    // in the div above. we're going to need to build these into a grid
    // and then trigger it to load page 2

    // the form_data var needs to be fed into the ajax function to do our
    // query - this is going to be expanded to include additional filters

    //console.log(form_data);


    jQuery.ajax({
        url: "/wp-json/research/get_research_posts",
        method: "POST",
        // the data value here is going to be formatted like a wp_query
        // we are going to expand it to contain meta_queries
        data: form_data,
    }).done(function (results) {

        let result_counter = jQuery(".cew-result-counter, .cew-scroll-to-top");
        if (results.data.length) {
            content.removeClass("no-results");
            let html = '';
            result_counter.show();


            if (page_no <= results.max_num_pages) {

                let r_count = result_counter.find('h5');

                r_count.text(results.found_posts);


                results.data.forEach(card => {
                    html += card.replace('class="cew-resource-card"', 'class="cew-resource-card loading"');
                });

                if (action == 'submit') {
                    content.html(html).find('.loading').removeClass('loading');
                    content.fadeIn();
                } else {
                    content.append(html).find('.loading').fadeIn().removeClass('loading');
                }

                if (page_no == results.max_num_pages) {
                    //Max page number reached
                    max_page_reached = true;
                    page_no = results.max_num_pages;
                } else {
                    max_page_reached = false;
                }
            } else {
                //Max page number reached
                max_page_reached = true;
            }

        } else {
            content.addClass("no-results");
            result_counter.hide();
            if (results.max_num_pages > 0) {
                page_no = page_no > 1 ? page_no - 1 : page_no;
            } else {
                page_no = 1;
            }
        }

        if (action == 'submit') {
            jQuery('body').removeClass('form-is-loading');
            jQuery('.spinner').removeClass('form-loading').addClass('hidden');
        } else {
            jQuery('body').removeClass('scroll-is-loading');
            jQuery('.load-more').addClass("hidden");
        }

        if (!results.data.length) {
            html = '<p class="text-center">No results found</p>';

            if (action == 'submit' || action == 'load') {
                content.html(html).fadeIn();
            } else {
                content.append(html).fadeIn();
            }
        }


        let first_card = content.find(".cew-resource-card:first-of-type"),
            card_height = first_card.outerHeight(),
            desktop_estimate_height = card_height * results.card_rows[1],
            tablet_estimate_height = card_height * results.card_rows[0],
            mobile_estimate_height = card_height * results.found_posts,
            w_width = jQuery(window).width();


        let page = results.query_args.paged;
        let ppp = 12;
        let displayed_posts = page * ppp;
        let remaining_posts = results.found_posts - displayed_posts;


        let ppr = 1;
        if (w_width >= 1000) ppr = 2;
        if (w_width >= 1200) ppr = 3;

        let remaining_rows = Math.ceil(remaining_posts / ppr);

        let estimated_remainder = remaining_rows * card_height;
        let estimated_total_height = content.height() + estimated_remainder;

        if (remaining_rows >= 1)
            content.attr('data-total-height', estimated_total_height);


        action = 'submit';	// Reset

        jQuery(".cew-resource-card").keypress(function (e) {
            if (e.which === 13)
                jQuery(this).trigger("click");
        });


    }).fail(function (error) {
        //console.log(error.responseText);
    });
}

function init_box_update() {

    let counter = jQuery(".cew-result-counter");

    if (!counter.length) return;

    let content = jQuery(".cew-resource-library-container"),
        circle = counter.find("path.progress"),
        boxes = jQuery(".cew-result-counter, .cew-scroll-to-top");

    jQuery(window).on('scroll', function () {
        let box = content[0].getBoundingClientRect(),
            w_height = jQuery(window).height(),
            w_width = jQuery(window).width(),
            start_trigger = w_height * .65,
            total_height = parseFloat(content.attr('data-total-height'));


        if (box.y > start_trigger) {
            circle.attr("stroke-dasharray", "0, 100");
            boxes.addClass("hidden");
        }


        if (box.y <= start_trigger) {
            let val = Math.floor(((start_trigger - box.y) / total_height) * 100);
            boxes.removeClass("hidden");
            circle.attr("stroke-dasharray", val + ", 100");
        }

    });

    jQuery('.cew-scroll-to-top').click(function (e) {
        let new_position = jQuery('.cew-resource-query-header').offset();
        jQuery('html, body').stop().animate({scrollTop: new_position.top - 300}, 500);
        e.preventDefault();
    });

}

function init_search() {

    let form = jQuery("form#cew-resource-query");
    if (!form.length) return;


    let select = form.find(".cew-select");


    select.each(function () {
        jQuery(this).select2({
            closeOnSelect: false,
            width: '100%',
        });
    });


    let sort_order = jQuery("#cew-resource-query-sort-order"),
        so_container = sort_order.parent(),
        screen = so_container.find(".screen");


    sort_order.on("change", function () {
        so_container.toggleClass("active");
    });

    screen.on("click", function () {
        let c = sort_order.get(0);
        c.checked = !c.checked;
        //console.log(c);
        sort_order.trigger("change");
    });


    if (is_cew_home())
        return true;

    // from here out, we're only doing things that happen on the full page

    form.on('submit', function (event) {
        event.preventDefault();
        execute_search(event, form);
    });


    // we keep this separate from the above the home check
    // because we have the search on the home, but it doesn't
    // auto update
    sort_order.on("change", function () {
        form.trigger("submit");
    });

    let preselected = jQuery('.research_area_checkbox_container.preselected');


    if (!preselected.length)
        submit_report_search();


}

function init_select2_default_text() {
    let form = jQuery("form#cew-resource-query");
    if (!form.length) return;

    let select = form.find(".cew-select");


    select.each(function () {
        let obj = jQuery(this).find(".select2-offscreen");
        obj.html(obj.prev().text());
    });
}

function change_button_state(obj) {


    let checkbox_container = obj,
        checkbox = checkbox_container.find("input")[0],
        color = checkbox_container.attr("data-bg-color");

    if (checkbox.disabled === true)
        return false;

    checkbox_container.toggleClass('selected');
    if (checkbox_container.hasClass('selected')) {
        checkbox_container.css("background-color", color);
        checkbox_container.attr('data-aria-label', 'active');
        checkbox[0] = true;
    } else {
        checkbox_container.css("background-color", 'transparent');
        checkbox_container.attr('data-aria-checked', 'inactive');
        checkbox[0] = false;
    }

    submit_report_search();
}

function init_filters() {


    let preselected = jQuery('.research_area_checkbox_container.preselected'),
        checkbox = jQuery(".research_area_checkbox_container input[type=checkbox]");

    checkbox.on('change', function () {
        change_button_state(jQuery(this).parent());
    });


    if (preselected.length) {
        preselected.find("input")[0].checked = true;
        change_button_state(preselected);
    }
}

function checkbox_listen() {
    let input = jQuery("input[type=checkbox]");

    input.focus(function () {
        jQuery(this).parent().addClass("aria-active");
    });
    input.blur(function () {
        input.parent().removeClass("aria-active");
    });
}

function init_dd_listen() {


    let dd = jQuery("select[name=publication_year],select[name=research_type]");
    if (!dd.length) return false;


    if (is_cew_home())
        return false;

    dd.on('change', function () {
        submit_report_search();
    })


}

function submit_report_search() {

    let submit = jQuery('form#cew-resource-query input[type="submit"]');

    if (!submit.length) return false;

    submit.trigger('click');
}

function init_reset_button() {

    let button = jQuery('.cew-reset-filter');

    if (!button.length) return;

    button.on('click', function (e) {
        e.preventDefault();
        var form = jQuery("form#cew-resource-query");

        form[0].reset();	// do a basic form reset, just in case

        // trigger element's 'change' event to "notify" 'selec2' or any scripts that are bind to the element.
        form.find('[name=report_title]').val('').change();
        form.find('[name=research_type]').val('').change();
        form.find('[name=keyword]').val('');
        form.find('[name=publication_year]').val('').change();
        form.find('[type=checkbox], [type=radio]').prop('checked', false);
        let selected = form.find(".research_area_checkbox_container.selected");
        selected.each(function () {
            change_button_state(jQuery(this));
        });
    });
}

function init_home_research_box_init() {
    let container = jQuery(".cew-home-research-type-container");

    if (!container.length) return;

    let box = container.find(".boxshadow"),
        height = 0;


    box.each(function () {
        let obj_height = jQuery(this).height();
        height = obj_height > height ? obj_height : height;
    });

    box.height(height + "px");

}

function set_default_header() {


    let default_icon = jQuery('.cew-resource-hero').data('default-img'),
        default_name = jQuery('.cew-resource-hero').data('default-name'),
        default_desc = jQuery('.cew-resource-hero').data('default-desc');


    jQuery('#cew-resource-query [name="research_type"]').on('change', function () {
        var icon = jQuery(this).find(':selected').data('icon');
        var name = jQuery(this).find(':selected').data('name');
        var description = jQuery(this).find(':selected').data('description');

        jQuery('.cew-resource-hero .cew-resource-hero-img').html('<img src="' + (icon ? icon : default_icon) + '" />');
        jQuery('.cew-resource-hero .cew-resource-hero-heading').html((name ? name : default_name));
        jQuery('.cew-resource-hero .cew-resource-hero-desc').html((description ? description : default_desc));
    });

}

function cew_copy_url(text) {
    const el = document.createElement('textarea');
    el.value = text;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}

function init_mobile_media_filter_toggle() {

    let toggle = jQuery(".cew-filter-display-toggle");
    if (!toggle.length) return;

    let container = jQuery('.cew-checkbox-filter-container'),
        text = toggle.text(),
        closeText = "Click to Hide Media Filters",
        small = 700,
        w = jQuery(window).width(),
        toggleContainer = toggle.parent(),
        isSmall = w <= small;

    toggle.addClass("active");


    function activate_filter() {
        container.slideToggle();
        let _t = toggle.hasClass("active") ? text : closeText;
        toggle.toggleClass("active")
            .text(_t);
    }

    toggle.bind("click", activate_filter);


    function prep_mobile() {

        let w = jQuery(window).width(),
            isSmall = w <= small;

        if (isSmall) {

            toggleContainer.show();
            container.hide();

            toggle.removeClass("active")
                .text(text);

            return true;

        }

        toggleContainer.hide();
        container.show();
        toggle.addClass("active");

    }

    prep_mobile();

    jQuery(window).resize(function () {
        prep_mobile()
    });

}

function scroll_func() {

    let body = jQuery('body'),
        container = jQuery('.cew-resource-library-container');

    // check to see if the search box is ready to receive another search
    let ready_for_more = !max_page_reached &&
        !container.hasClass("no-results") &&
        !body.hasClass('scroll-is-loading') &&
        !body.hasClass('form-is-loading');

    if (!ready_for_more) return false;

    let diff = jQuery(document).height() - jQuery(this).height() - jQuery(this).scrollTop();

    //Check if current scroll reached bottom of the page...
    let scroll_has_reached_bottom = diff <= 150;

    if (!scroll_has_reached_bottom) return false;


    action = 'load';
    page_no++;


    if (!is_cew_home()) {
        body.addClass('scroll-is-loading');
        submit_report_search();
    }
}


function init_home_page_behaviors() {
    // i dont think we're making use of
    // these content blocks anymore

    jQuery(".cew-research-button").attr("tabindex", "0");

    jQuery(".cew-research-type").on("keypress", function (e) {
        if (e.which === 13)
            jQuery(this).click();
    })

}


jQuery(function () {
    init_home_page_behaviors();
    set_default_header();
    init_mobile_media_filter_toggle();
    init_search();
    init_dd_listen();
    init_filters();
    init_reset_button();
});


jQuery(window).resize(function () {
    init_home_research_box_init();
});

jQuery(window).on('scroll', function () {
    scroll_func();
});

jQuery(window).on("load", function () {
    init_home_research_box_init();
    init_box_update();
    init_select2_default_text();
    checkbox_listen();
});

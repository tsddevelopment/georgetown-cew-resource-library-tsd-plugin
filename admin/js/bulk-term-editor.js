
jQuery(document).ready(function () {

    jQuery('table.object-list input[type="checkbox"]').on('change', function () {

        var checkbox = jQuery(this);

        if (checkbox.is(':checked')) {
            var enabled = 1;
        } else {
            var enabled = 0
        }

        var post_id = checkbox.data('post-id');
        var tax_name = checkbox.data('tax');
        var term_id = checkbox.data('term');

        checkbox.prop('disabled', true);

        jQuery.ajax({
            url: 'https://thememigration.wpengine.com/wp-admin/admin-ajax.php',
            type: 'post',
            dataType: 'json',
            data: {
                action: 'tsd_update_post_term',
                pid: post_id,
                taxname: tax_name,
                termid: term_id,
                status: enabled,
            },
            success: function (response) {
                // console.log( response );
            },
            complete: function () {
                checkbox.prop('disabled', false);
            }
        });

    });

});
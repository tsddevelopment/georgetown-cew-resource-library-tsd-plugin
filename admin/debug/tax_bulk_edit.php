<?php

$pt = [];
$pts = ['resource', 'cew_reports'];

$op = '';

foreach ($pts as $p)
    $pt[] = get_post_type_object($p);

foreach ($pt as $c => $type):

    $name = $type->name;
    $taxonomy_objects = get_object_taxonomies($name, 'objects');


    $query = new \WP_Query(['posts_per_page' => -1, 'post_type' => $name, 'orderby' => 'title','order' => 'ASC']);


    $grouped_terms = [];

    foreach ($taxonomy_objects as $tax):
        $grouped_terms[$tax->name] = get_terms($tax->name, ['hide_empty' => 0]);
    endforeach;


    $op .= "<h3>$name</h3>";

    foreach ($grouped_terms as $group => $terms):


        if (in_array($group, ['category', 'cew_reports_taxonomy'])):
            $op .= "<h6>Non-Project Category Hidden</h6>";
            continue;
        endif;


        $op .= "<h4 id='{$group}_{$name}'>$group in post type $name</h4>";

        $op .= "<table class='object-list'>";

        $op .= "<thead>";
        $op .= "<tr>";


        $op .= "<td><p>post_title</p></td>";

        foreach ($terms as $term):

            $url = "https://thememigration.wpengine.com/wp-admin/term.php?taxonomy={$term->taxonomy}&tag_ID={$term->term_id}";
            $edit_link = "<a href='$url' target='_blank' style='font-size: 8px;'>edit</a>";

            $op .= "<td>";
            $op .= "<p>$term->name $edit_link</p>";
            $op .= "</td>";

        endforeach;


        $op .= "</tr>";
        $op .= "</thead>";


        foreach ($query->posts as $post):

            $url = "https://thememigration.wpengine.com/wp-admin/post.php?post={$post->ID}&action=edit";
            $edit_link = "<a href='$url' target='_blank' style='font-size: 8px;'>edit</a>";

            $op .= "<tr>";
            $op .= "<td>";
            $op .= "<p>$post->post_title $edit_link</p>";
            $op .= "</td>";

            // Get all the current term of the post
            $active_terms = [];
            if ($post_terms = get_the_terms($post->ID, $group)) {
                foreach ($post_terms as $post_term) {
                    $active_terms[] = $post_term->term_id;
                }
            }


            foreach ($terms as $term):

                $op .= "<td>";
                $op .= "<input type='checkbox' data-tax='{$tax->name}' data-term='{$term->term_id}' data-post-id='{$post->ID}' " . (in_array($term->term_id, $active_terms) ? 'checked="checked"' : '') . " />";
                $op .= "</td>";

            endforeach;


            $op .= "</tr>";


        endforeach;
        $op .= "</table>";
    endforeach;


endforeach;


print $op;

//print_r($taxonomy_objects);


?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/wp-content/plugins/gcew-resource-library/admin/js/bulk-term-editor.js"></script>

<style>
    td {
        padding: 3px;
        max-width: 300px;
    }

    thead td {
        font-weight: bold;
    }

    tr td:first-child {
        padding: 3px 0;
    }

    tr:nth-of-type(odd) {
        background: #efefef;
    }
</style>
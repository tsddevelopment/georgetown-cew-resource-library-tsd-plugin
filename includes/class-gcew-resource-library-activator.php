<?php

/**
 * Fired during plugin activation
 *
 * @link       https://topshelfdesign.net
 * @since      1.0.0
 *
 * @package    Gcew_Resource_Library
 * @subpackage Gcew_Resource_Library/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gcew_Resource_Library
 * @subpackage Gcew_Resource_Library/includes
 * @author     Top Shelf Design <info@topshelfdesign.net>
 */
class Gcew_Resource_Library_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}

<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://topshelfdesign.net
 * @since      1.0.0
 *
 * @package    Gcew_Resource_Library
 * @subpackage Gcew_Resource_Library/includes
 */

define("PLUGIN_DIR", plugin_dir_path(dirname(__FILE__)));

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Gcew_Resource_Library
 * @subpackage Gcew_Resource_Library/includes
 * @author     Top Shelf Design <info@topshelfdesign.net>
 */
class Gcew_Resource_Library {

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Gcew_Resource_Library_Loader $loader Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $plugin_name The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $version The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct() {
        if (defined('GCEW_RESOURCE_LIBRARY_VERSION')) {
            $this->version = GCEW_RESOURCE_LIBRARY_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        $this->plugin_name = 'gcew-resource-library';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();


        add_image_size('Featured Card', 350, 450, 1);
        add_image_size('Resource Card', 900, 900, true);

        add_action('init', [$this, 'parse_url']);
        add_action('wp', [$this, 'rewrite_rules']);
        add_action('query_vars', [$this, 'add_query_vars']);
        add_action('parse_request', [$this, 'parse_request']);

        $report_page_opts = ['page_title' => 'Resource Library Card Options',
                             'menu_title' => 'Resource Card Options',
                             'menu_slug'  => 'report-option-settings',
                             'capability' => 'edit_posts',
                             'redirect'   => false];

         \acf_add_options_page($report_page_opts);

    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Gcew_Resource_Library_Loader. Orchestrates the hooks of the plugin.
     * - Gcew_Resource_Library_i18n. Defines internationalization functionality.
     * - Gcew_Resource_Library_Admin. Defines all hooks for the admin area.
     * - Gcew_Resource_Library_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies() {

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-gcew-resource-library-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-gcew-resource-library-i18n.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-gcew-resource-library-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-gcew-resource-library-public.php';

        $this->loader = new Gcew_Resource_Library_Loader();

    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Gcew_Resource_Library_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale() {

        $plugin_i18n = new Gcew_Resource_Library_i18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');

    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks() {

        $plugin_admin = new Gcew_Resource_Library_Admin($this->get_plugin_name(), $this->get_version());

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
        $this->loader->add_action('init', $plugin_admin, 'tsd_add_vc_modules');

    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks() {

        $plugin_public = new Gcew_Resource_Library_Public($this->get_plugin_name(), $this->get_version());

        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts', 99);



        \TSD\Acme::create_shortcode('cew_about_us');
        \TSD\Acme::create_shortcode('cew_home_resources');
        \TSD\Acme::create_shortcode('cew_home_research');
        \TSD\Acme::create_shortcode('cew_resource_hero');
        \TSD\Acme::create_shortcode('cew_resource_search');
        \TSD\Acme::create_shortcode('vc_module');
        \TSD\Acme::create_shortcode('cew_report_resource_tiles');

        add_shortcode("tsd_vc_module", function($atts, $content, $tag) {
            ob_start();
            include(PLUGIN_DIR . 'shortcode/tsd_vc_module.php');
            return ob_get_clean();
        });

    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run() {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name() {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Gcew_Resource_Library_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader() {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version() {
        return $this->version;
    }

    public function add_query_vars($vars) {

        $vars[] = 'report_title';
        $vars[] = 'research_area';
        $vars[] = 'report_keyword';
        $vars[] = 'media_type';
        return $vars;
    }

    public function parse_url() {

        global $wp_rewrite;

        $wp_rewrite->add_rule('^resources/?$', 'index.php?pagename=cew-report-library&media_type=webpage', false);

        $wp_rewrite->add_rule('^resources/all/?$', 'index.php?pagename=cew-report-library', false);
        $wp_rewrite->add_rule('^resources/reports/?$', 'index.php?pagename=cew-report-library&media_type=webpage', false);
        $wp_rewrite->add_rule('^resources/pdf/?$', 'index.php?pagename=cew-report-library&media_type=report-pdf', false);
        $wp_rewrite->add_rule('^resources/report-overview/?$', 'index.php?pagename=cew-report-library&media_type=report-pdf', false);
        $wp_rewrite->add_rule('^resources/journal-article/?$', 'index.php?pagename=cew-report-library&media_type=journal-articles', false);
        $wp_rewrite->add_rule('^resources/journal-articles/?$', 'index.php?pagename=cew-report-library&media_type=journal-articles', false);
        $wp_rewrite->add_rule('^resources/book-chapter/?$', 'index.php?pagename=cew-report-library&media_type=book-chapter', false);
        $wp_rewrite->add_rule('^resources/book-chapters/?$', 'index.php?pagename=cew-report-library&media_type=book-chapter', false);
        $wp_rewrite->add_rule('^resources/newsletter/?$', 'index.php?pagename=cew-report-library&media_type=newsletter', false);
        $wp_rewrite->add_rule('^resources/newsletters/?$', 'index.php?pagename=cew-report-library&media_type=newsletter', false);
        $wp_rewrite->add_rule('^resources/state-summary/?$', 'index.php?pagename=cew-report-library&media_type=state_summary', false);
        $wp_rewrite->add_rule('^resources/blog/?$', 'index.php?pagename=cew-report-library&media_type=blog', false);
        $wp_rewrite->add_rule('^resources/video/?$', 'index.php?pagename=cew-report-library&media_type=video', false);
        $wp_rewrite->add_rule('^resources/videos/?$', 'index.php?pagename=cew-report-library&media_type=video', false);
        $wp_rewrite->add_rule('^resources/executive-summary/?$', 'index.php?pagename=cew-report-library&media_type=executive-summary', false);
        $wp_rewrite->add_rule('^resources/press-release/?$', 'index.php?pagename=cew-report-library&media_type=press-release', false);
        $wp_rewrite->add_rule('^resources/press-releases/?$', 'index.php?pagename=cew-report-library&media_type=press-release', false);
        $wp_rewrite->add_rule('^resources/powerpoint/?$', 'index.php?pagename=cew-report-library&media_type=powerpoint', false);
        $wp_rewrite->add_rule('^resources/interactive-tool/?$', 'index.php?pagename=cew-report-library&media_type=interactive-tool', false);
        $wp_rewrite->add_rule('^resources/infographic/?$', 'index.php?pagename=cew-report-library&media_type=infographic', false);

        $wp_rewrite->add_rewrite_tag("%report_title%", '([^/]+)', 'pagename=cew-report-library&report_title=');
        $wp_rewrite->add_permastruct('resources-by-title', '/resources/%report_title%', false);

        $wp_rewrite->add_rewrite_tag("%research_area%", '([^/]+)', 'pagename=cew-report-library&research_area=');
        $wp_rewrite->add_permastruct('resources-by-area', '/resources/research/%research_area%', false);

        $wp_rewrite->add_rewrite_tag("%report_keyword%", '([^/]+)', 'pagename=cew-report-library&report_keyword=');
        $wp_rewrite->add_permastruct('resources-by-keyword', '/resources/search/%report_keyword%', false);

        $wp_rewrite->add_rewrite_tag("%media_type%", '([^/]+)', 'pagename=cew-report-library&media_type=');
        $wp_rewrite->add_permastruct('resources-by-media-type', '/resources/media/%medila_type%', false);

        $wp_rewrite->add_rewrite_tag("%tags%", '([^/]+)', 'pagename=cew-report-library&report_tag=');
        $wp_rewrite->add_permastruct('resources-by-tags', '/resources/tags/%tags%', false);


        flush_rewrite_rules();

    }

    public function rewrite_rules() {

        // NOTE: Flush permalinks whenever this gets changed.

        $page_id = get_the_ID();

        $is_lib = get_the_ID() == 16943;


    }

    public function parse_request() {

        /* $this->gcew_write_log( $_GET );
        $this->gcew_write_log( $_POST ); */

    }

    function gcew_write_log($msg) {
        $file = dirname(__FILE__) . '/gcew_log.log';

        $date = date('m/d/Y h:i:s a');

        if (is_array($msg) || is_object($msg)) {
            $msg = print_r($msg, true);
        }

        $msg = '[ ' . $date . ' ]: ' . $msg;
        $msg .= "\r\n\r\n";

        // Write the contents to the file,
        // using the FILE_APPEND flag to append the content to the end of the file
        // and the LOCK_EX flag to prevent anyone else writing to the file at the same time
        file_put_contents($file, $msg, FILE_APPEND | LOCK_EX);
    }
}

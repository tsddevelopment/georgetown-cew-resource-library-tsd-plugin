<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       https://topshelfdesign.net
 * @since      1.0.0
 *
 * @package    Gcew_Resource_Library
 * @subpackage Gcew_Resource_Library/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Gcew_Resource_Library
 * @subpackage Gcew_Resource_Library/includes
 * @author     Top Shelf Design <info@topshelfdesign.net>
 */
class Gcew_Resource_Library_Loader {

	/**
	 * The array of actions registered with WordPress.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      array    $actions    The actions registered with WordPress to fire when the plugin loads.
	 */
	protected $actions;

	/**
	 * The array of filters registered with WordPress.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      array    $filters    The filters registered with WordPress to fire when the plugin loads.
	 */
	protected $filters;

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->actions = array();
		$this->filters = array();

	}

	/**
	 * Add a new action to the collection to be registered with WordPress.
	 *
	 * @since    1.0.0
	 * @param    string               $hook             The name of the WordPress action that is being registered.
	 * @param    object               $component        A reference to the instance of the object on which the action is defined.
	 * @param    string               $callback         The name of the function definition on the $component.
	 * @param    int                  $priority         Optional. The priority at which the function should be fired. Default is 10.
	 * @param    int                  $accepted_args    Optional. The number of arguments that should be passed to the $callback. Default is 1.
	 */
	public function add_action( $hook, $component, $callback, $priority = 10, $accepted_args = 1 ) {
		$this->actions = $this->add( $this->actions, $hook, $component, $callback, $priority, $accepted_args );
	}

	/**
	 * Add a new filter to the collection to be registered with WordPress.
	 *
	 * @since    1.0.0
	 * @param    string               $hook             The name of the WordPress filter that is being registered.
	 * @param    object               $component        A reference to the instance of the object on which the filter is defined.
	 * @param    string               $callback         The name of the function definition on the $component.
	 * @param    int                  $priority         Optional. The priority at which the function should be fired. Default is 10.
	 * @param    int                  $accepted_args    Optional. The number of arguments that should be passed to the $callback. Default is 1
	 */
	public function add_filter( $hook, $component, $callback, $priority = 10, $accepted_args = 1 ) {
		$this->filters = $this->add( $this->filters, $hook, $component, $callback, $priority, $accepted_args );
	}

	/**
	 * A utility function that is used to register the actions and hooks into a single
	 * collection.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @param    array                $hooks            The collection of hooks that is being registered (that is, actions or filters).
	 * @param    string               $hook             The name of the WordPress filter that is being registered.
	 * @param    object               $component        A reference to the instance of the object on which the filter is defined.
	 * @param    string               $callback         The name of the function definition on the $component.
	 * @param    int                  $priority         The priority at which the function should be fired.
	 * @param    int                  $accepted_args    The number of arguments that should be passed to the $callback.
	 * @return   array                                  The collection of actions and filters registered with WordPress.
	 */
	private function add( $hooks, $hook, $component, $callback, $priority, $accepted_args ) {

		$hooks[] = array(
			'hook'          => $hook,
			'component'     => $component,
			'callback'      => $callback,
			'priority'      => $priority,
			'accepted_args' => $accepted_args
		);

		return $hooks;

	}

	/**
	 * Register the filters and actions with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {

		foreach ( $this->filters as $hook ) {
			add_filter( $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );
		}

		foreach ( $this->actions as $hook ) {
			add_action( $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );
		}


        add_action( 'wp_ajax_tsd_update_post_term', 'tsd_update_post_term' );
        add_action( 'wp_ajax_nopriv_tsd_update_post_term', 'tsd_update_post_term' ); // ---> Disable if the ajax should only be called with a user logged in.
        function tsd_update_post_term() {

            if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

                $result = array();

                $post_id = isset( $_POST['pid'] ) ? $_POST['pid'] : 0;
                $tax_name = isset( $_POST['taxname'] ) ? $_POST['taxname'] : '';
                $term_id = isset( $_POST['termid'] ) ? (int) $_POST['termid'] : 0;
                $status = isset( $_POST['status'] ) ? (bool) $_POST['status'] : false;

                if ( term_exists( $term_id, $tax_name ) ) {
                    if ( $status ) {
                        $result = wp_set_object_terms( $post_id, $term_id, $tax_name, true );
                    } else {
                        $result = wp_remove_object_terms( $post_id, $term_id, $tax_name );
                    }
                }

                $is_error = is_wp_error( $result );

                echo json_encode( array(
                    'status' => ! $is_error ? 'ok' : $result->get_error_message(),
                    'data' => ! $is_error ? $result : '',
                ) );

            }

            die();

        }



        if (isset($_GET['tax_bulk_edit'])):


            add_action("wp_loaded", function() {
                include $_SERVER['DOCUMENT_ROOT']  .'/wp-content/plugins/gcew-resource-library/admin/debug/tax_bulk_edit.php';
                die();
            });


        endif;

	}

}

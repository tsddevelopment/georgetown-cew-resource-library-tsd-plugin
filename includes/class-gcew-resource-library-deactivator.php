<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://topshelfdesign.net
 * @since      1.0.0
 *
 * @package    Gcew_Resource_Library
 * @subpackage Gcew_Resource_Library/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gcew_Resource_Library
 * @subpackage Gcew_Resource_Library/includes
 * @author     Top Shelf Design <info@topshelfdesign.net>
 */
class Gcew_Resource_Library_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}

<?php

namespace TSD;


use Kint\Kint;

class Acme {

    static public function get_file( $filepath, $vars = [] ) {
        ob_start();
        foreach ( $vars as $var => $value )
            $$var = $value;

        include( $_SERVER['DOCUMENT_ROOT'] . '/wp-content/plugins/gcew-resource-library/includes/' . $filepath );

        return ob_get_clean();
    }

    public static function create_shortcode( $name, $vars = [] ) {

        add_shortcode( $name, function( $atts, $content, $tag ) use ( $name, $vars ) {

            $data = [ 'atts' => $atts, 'content' => $content, 'tag' => $tag ];

            $filename = "components/shortcodes/{$name}.php";

            return Acme::get_file( $filename, $data );
        } );

    }

    private static function run_query( $opts, $vars ) {

        API::tsd_write_log( $opts );
        API::tsd_write_log( $vars );

        $page_no = isset( $_POST['page_no'] ) && $_POST['page_no'] ? $_POST['page_no'] : 1;

        $vars['paged'] = $page_no;

        if ( false ):
            $vars['meta_query']['post_link'] = [ 'key'     => 'post_link',
                                                 'value'   => '',
                                                 'compare' => '=' ];

            $vars['meta_query']['embed_url'] = [ 'key'     => 'embed_url',
                                                 'value'   => '',
                                                 'compare' => '=' ];

            $vars['meta_query']['file_embed'] = [ 'key'     => 'file_embed',
                                                  'value'   => '',
                                                  'compare' => '=' ];
            $vars['posts_per_page']           = - 1;
        endif;


        if ( $opts['use_swp_query'] && class_exists( 'SWP_Query' ) && array_key_exists( 's', $vars ) && $vars['s'] ) {    //Only use SWP_Query if 'use_swp_query' is enabled and if there's a keyword specified.

            $vars['engine'] = 'library_query'; // taken from the SearchWP settings screen

            $query = new \SWP_Query( $vars ); // see all args at https://searchwp.com/docs/swp_query/

        }
        else {
            $query = new \WP_Query( $vars );
        }

        API::tsd_write_log( $query->found_posts );


        return $query;
    }

    public static function get_library( $user_vars = [], $user_opts = [] ) {

        // the library is the combination of the resource posts and the
        // report posts. we should not include the reports if we have
        // specified a media type filter, and have not specified the report
        // webpage toggle

        // the report webpage toggle will include the report posts in with the
        // query - the default is to have them included


        $default_vars = [ 'post_type'      => [ 'resource', 'cew_reports' ],
                          'posts_per_page' => 12 ];

        $default_opts = [ 'return_query' => false, 'use_swp_query' => true ];

        $vars = wp_parse_args( $user_vars, $default_vars );
        $opts = wp_parse_args( $user_opts, $default_opts );

        $display_reports     = array_key_exists( 'report_webpage', $_POST );
        $media_type_selected = array_key_exists( "media_type", $_POST );


        // in the event that we we have selected a media type filter, but
        // the report webpage isn't chosen, then we only want to search for
        // resources

        if ( $media_type_selected && ! $display_reports )
            $vars['post_type'] = 'resource';

        // in the event that we have the report filter selected, but don't
        // have any of the other media types specified, then we just want
        // to query the reports

        if ( ! $media_type_selected && $display_reports )
            $vars['post_type'] = 'cew_reports';

        // if both reports and resources are desired, we need to split
        // them into two queries - one for the filtered resources and one
        // for all the reports. we set the post type here to be just resources
        // and we're going to rerun the query after we run it the first time
        // ALSO it needs to have a posts per page of -1 so that we can cycle
        // through all the returned posts

        if ( $media_type_selected && $display_reports ):
            $vars['post_type']      = 'resource';
            $vars['posts_per_page'] = - 1;
            unset( $vars['paged'] );
        endif;


        $query = self::run_query( $opts, $vars );

        if ( $media_type_selected && $display_reports ):

            // now we just get all the reports, pool all the ids together from both
            // queries and run a third query that just feeds in the ids so that
            // we will finish with a single, merged, query obj

            $pooled_ids = [];

            foreach ( $query->posts as $r )
                $pooled_ids[] = $r->ID;

            $vars['post_type'] = 'cew_reports';

            // cew_report can have a research area - so we need to manually unset
            // the other taxonomy types so that we can filter the research areas
            unset( $vars['tax_query']['media_type'] );
            unset( $vars['tax_query']['report_tag'] );
            $vars['posts_per_page'] = - 1;

            $report_query = self::run_query( $opts, $vars );

            foreach ( $report_query->posts as $cr )
                $pooled_ids[] = $cr->ID;


            $query_has_posts = count( $pooled_ids );

            if ( $query_has_posts ):
                unset( $vars['s'] );
                $vars['post_type'] = 'any';
                $vars['post__in']  = $pooled_ids;
            else:

                // this presents us with a problem: if there are no posts
                // in either of the two grouped posts, we need to return
                // a query that we know won't have any posts. The alternative
                // it seems is to return a query with all posts. Not desirable.

                // So - it can return any resource or report it wants, as long
                // as it's the default wordpress 'hello world' post, which will
                // return an empty result

                $vars['post__in'] = [1];


            endif;


            $vars['posts_per_page'] = 12;
            // now that we're grouped all together, we need to strip all the filters
            // out because the pooled ids were gathered via filtered searches that
            // may not apply across the two post types
            unset( $vars['tax_query'] );


            $query = self::run_query( $opts, $vars );

            $t = [];

            foreach ( $query->posts as $p )
                $t[] = $p->ID;


        endif;

        $return_val = $query->posts;

        if ( $opts['return_query'] )
            $return_val = $query;

        return $return_val;

    }

    public static function get_research_areas() {
        $return = get_terms( [ 'taxonomy'   => 'research_area',
                               'hide_empty' => false, ] );

        return $return;
    }

    public static function get_media_type() {
        $return = get_terms( [ 'taxonomy'   => 'media_type',
                               'hide_empty' => false, ] );

        return $return;
    }

    public static function get_publication_years() {

        global $wpdb;
        $sql_string = "select distinct YEAR(CAST(post_date AS DATE)) as post_year
            from {$wpdb->posts}
            where post_type in('cew_reports', 'resource')
            order by post_year desc";

        $years = $wpdb->get_results( $sql_string );

        return $years;

    }

    public static function get_reports( $user_vars = [] ) {

        $default_vars = [ 'post_type'      => [ 'cew_reports' ],
                          'posts_per_page' => - 1 ];


        $vars = wp_parse_args( $user_vars, $default_vars );


        $query = new \WP_Query( $vars );

        $return_val = $query->posts;

        return $return_val;
    }

    public static function library_query_form( $atts = [] ) {

        $default = [ 'type_filter' => false ];

        $config = wp_parse_args( $atts, $default );

        $filter_html = '';
        $reset_html  = '';


        if ( $config['type_filter'] ):

            $resource_types = self::get_media_type();

            $resource_type_filters = '';

            $media_type_selected = [];
            if ( isset( $config['search_filter']['media_type'] ) )
                $media_type_selected = $config['search_filter']['media_type'];

            // we've gotta make a report button

            foreach ( $resource_types as $resource_type ):
                $color   = get_field( "color", "research_area_" . $resource_type->term_id );
                $icon    = get_field( "icon", "media_type_" . $resource_type->term_id );
                $iconUrl = $icon['url'];

                $svg = $icon ? file_get_contents( $iconUrl ) : "[i]";

                $is_selected           = in_array( $resource_type->slug, $media_type_selected );
                $selectedClass         = $is_selected ? 'preselected' : '';
                $selectedProperty      = $is_selected ? 'checked' : '';
                $resource_type_filters .= "
                    <div class='research_area_checkbox_container $selectedClass' data-term='$resource_type->slug' data-bg-color='$color' id='resource_{$resource_type->slug}' >
                        <label for='filter_label_{$resource_type->slug}'>{$resource_type->name}</label>
                        <input aria-label='{$resource_type->name} Filter' type='checkbox' $selectedProperty name='media_type[]' value='{$resource_type->slug}' id='filter_label_{$resource_type->slug}'   /> 
                    </div>
                ";
            endforeach;

            $report_selected = get_query_var( "media_type" ) === 'webpage' ? 'preselected' : '';
            $report_color    = '#f9a12c';
            $report_slug     = 'report_webpage';
            $report_title    = 'Report Overview';

            $report_button = "<div class='research_area_checkbox_container $report_selected' data-term='report'  data-bg-color='$report_color' id='report_webpage'>
                <input type='checkbox' name='{$report_slug}' value='true'  aria-label='{$report_title} Filter' id='{$report_slug}_checkbox'/> 
                <label for='{$report_slug}_checkbox'>{$report_title}</label>
            </div>";

            $resource_type_filters = $report_button . $resource_type_filters;


            $filter_html = "<div class='col'>
                        <div class='vc_col-sm-12 pt-2 cew-resource-type-checkbox-filter' role='form' aria-label='Resource Library Filtering Options. Activate filters to view only relevant resources. The reset button will clear the form.'>
                                    <fieldset>
                                    <legend style='display: none;'>Media Type Toggles</legend>
                                        <p class='cew-filter-display-toggle-container'>
                                            <span class='cew-filter-display-toggle'>
                                                Click to Display Media Filters
                                            </span>
                                        </p>
                                        <div class='cew-checkbox-filter-container' id='resource_filters'>
                                                $resource_type_filters
                                        </div>
                                    </fieldset>
                                </div>
                            </div>";


            $reset_html = "<a class='button cew-reset-filter' href='javascript:void(0);' >Reset</a>";

        endif;

        $research_types    = self::get_research_areas();
        $publication_years = self::get_publication_years();

        $publication_year_options = '';
        $research_options         = '';

        $selected_year = '';
        if ( isset( $config['search_filter']['report_year'] ) )
            $selected_year = $config['search_filter']['report_year'];

        foreach ( $publication_years as $year ):
            $attr_selected = '';
            if ( array_key_exists( 'publication_year', $_POST ) && $_POST['publication_year'] !== '' )
                $attr_selected = $_POST['publication_year'] === $year->post_year ? 'selected' : '';
            $publication_year_options .= "<option " . selected( $selected_year, $year->post_year ) . " $attr_selected value='$year->post_year'>$year->post_year</option>";
        endforeach;

        $selected_option = '';
        if ( isset( $config['search_filter']['research_area'] ) )
            $selected_option = $config['search_filter']['research_area'];


        foreach ( $research_types as $research_type ) {
            $attr_selected = ( $selected_option ? selected( strtolower( $selected_option ), strtolower( $research_type->slug ), false ) : '' );
            if ( array_key_exists( 'research_type', $_POST ) && $_POST['research_type'] !== '' )
                $attr_selected = $_POST['research_type'] === $research_type->slug ? 'selected' : '';
            $research_icon    = get_field( 'icon', $research_type );
            $research_options .= "<option " . $attr_selected . " value='" . $research_type->slug . "' data-name='" . htmlentities( $research_type->name ) . "' data-description='" . htmlentities( $research_type->description ) . "' data-icon='" . esc_url( $research_icon ? $research_icon['url'] : '' ) . "'>$research_type->name</option>";
        }

        $report_keyword = '';
        if ( isset( $config['search_filter']['report_keyword'] ) )
            $report_keyword = $config['search_filter']['report_keyword'];

        if ( array_key_exists( 'keyword', $_POST ) && $_POST['keyword'] !== '' )
            $report_keyword = $_POST['keyword'];

        $action = \site_url( "resources" );

        if ( isset( $atts['action'] ) && $atts['action'] ) {
            $action = trailingslashit( '/' . ltrim( $atts['action'], '/\\' ) );
        }

        $html = "
             <form id='cew-resource-query' action='" . $action . "' method='post' role='form' aria-label='CEW Resource Library Filters'>
                 $filter_html
                 <div class='col filter-content'>
                    <div class='filter-option form-field-research'>
                        <label class='resource-query-field-label' for='cew-resource-query-topic-filter'>Topic</label>
                        <select class='cew-select' name='research_type' id='cew-resource-query-topic-filter'>
                            <option value=''>Select a Topic</option>
                            $research_options
                        </select>
                    </div>
                    <div class='filter-option form-field-report year'>
                        <label class='resource-query-field-label' for='cew-resource-query-date-filter'>Year</label>
                        <select class='cew-select' name='publication_year' id='cew-resource-query-date-filter'>
                            <option value=''>Select Year</option>
                            $publication_year_options
                        </select>
                    </div>
                    <div class='filter-option form-field-keyword'>
                        <label class='resource-query-field-label' for='cew-resource-query-keyword-filter'>Keyword</label>
                        <input type='text' name='keyword' id='cew-resource-query-keyword-filter' placeholder='Enter Keyword or Phrase' value='$report_keyword' >
                    </div>
                    <div class='form-field-buttons'>
                        <div class='order-container'>
                            <div class='sort-filter'>
                                <div class='screen'></div>
                                <input type='checkbox' id='cew-resource-query-sort-order' name='cew_order'>
                                <div class='cover'></div>
                            </div>
                        </div>
                        <div class='search-container'>
                            <input type='submit' class='button' value='Search' id='cew-resource-query-filter-submit'>
                        </div>
                        $reset_html
                    </div>
                </div>
            </form>";

        return $html;
    }

}
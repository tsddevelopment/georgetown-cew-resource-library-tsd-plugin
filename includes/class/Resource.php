<?php

namespace TSD;

class Resource {

    private $post;
    private $fields;

    public function __construct($post) {

        $this->post = $post;
        $this->fields = get_fields($post->ID);

    }

    public function get_card_html($template = 'card-excerpt') {
        return Acme::get_file("components/report/$template.php", ['post' => $this->post]);
    }


    public function get_post() {
        return $this->post;
    }

    public function get($field) {

        if(!array_key_exists($field, $this->fields))
            return null;

        return $this->fields[$field];

    }
}
<?php

namespace TSD;

class Card {

    private $post;
    private $type;
    private $fields;
    private $is_report = false;
    private $media_type = [];
    private $research_area = [];
    private $tn_size = 'Resource Card';

    public function __construct( $post ) {

        $this->post          = $post;
        $this->type          = $post->post_type;
        $this->is_report     = $this->type === 'cew_reports';
        $this->fields        = \get_fields( $post->ID );
        $this->media_type    = get_the_terms( $this->post, "media_type" );
        $this->research_area = get_the_terms( $this->post, "research_area" );


    }

    public function get_template_html( $template = 'default' ) {
        return Acme::get_file( "components/card/$template.php", [ 'card' => $this ] );
    }

    public function get_post() {
        return $this->post;
    }

    public function get_field( $field ) {

        if ( ! $this->fields )
            return null;

        if ( ! array_key_exists( $field, $this->fields ) )
            return null;

        return $this->fields[ $field ];

    }

    static function build_img( $img ) {
        $op = "
        <div class='featured-img' style='background-image: url($img);'></div>";

        return $op;
    }

    public function get_img( $tn = 'Featured Card' ) {
        $this->tn_size = $tn;

        return $this->featured_img( false );
    }

    public function featured_img( $build = true ) {


        global $wpdb;
        $post      = $this->post;
        $img_found = false;

        $reports = $wpdb->get_results( '
            select post_id
            from wp7610_postmeta
            where meta_key = "resources"
            and meta_value like "%\"' . $this->post->ID . '\";%"
        ' );


        if ( has_post_thumbnail( $post ) ):
            $img       = get_the_post_thumbnail_url( $post->ID, $this->tn_size );
            $img_found = true;
        endif;

        if ( count( $reports ) && ! $img_found ):
            if ( has_post_thumbnail( $reports[0]->post_id ) ):
                $img       = get_the_post_thumbnail_url( $reports[0]->post_id, $this->tn_size );
                $img_found = true;
            endif;
        endif;


        if ( $this->is_report ):
            if ( has_post_thumbnail( $this->post->ID ) ):
                $img       = get_the_post_thumbnail_url( $this->post->ID, $this->tn_size );
                $img_found = true;
            endif;
        endif;


        $tn_url = $this->get_video_tn_url();
        if ( $tn_url ):
            $img_found = true;
            $img       = $tn_url;
        endif;


        if ( ! $img_found && $this->media_type ):
            $acf_id    = "{$this->media_type[0]->taxonomy}_{$this->media_type[0]->term_id}";
            $img_field = get_field( "resource_card_image", $acf_id );
            $img       = $img_field['sizes'][ $this->tn_size ];
            $img_found = $img;
        endif;

        if ( ! $img_found ):
            $fallback = get_field( "resource_card_fallback_image", "option" );

            if ( $fallback ):
                $img       = $fallback['sizes']['large'];
                $img_found = true;
            endif;

        endif;


        if ( ! $img_found )
            $img = "http://placehold.it/600x400/144175/ffffff?text=CEW+Georgetown+Resource";

        return $build ? self::build_img( $img ) : $img;


    }

    public function get_video_tn_url() {

        // we're only doing this to resources
        // of media type = video
        // that don't have any reports

        if ( $this->is_report )
            return false;
        if ( $this->media_type[0]->slug !== 'video' )
            return false;
        if ( is_array( $this->fields['reports'] ) )
            return false;


        $link = $this->fields['post_link'];

        $yt_link      = strpos( $link, 'youtube' );
        $yt_shortlink = strpos( $link, 'youtu.be' );

        if ( ! $yt_link && ! $yt_shortlink )
            return false;

        preg_match( "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $link, $matches );

        if ( ! count( $matches ) )
            return false;


        $tn_url = "http://i3.ytimg.com/vi/{$matches[0]}/hqdefault.jpg";

        return $tn_url;

    }


    public function resource_icon() {


        $icon = false;
        if ( $this->media_type ):
            $acf_id = "{$this->media_type[0]->taxonomy}_{$this->media_type[0]->term_id}";
            $icon   = get_field( "icon", $acf_id );
            $color  = get_field( "color", $acf_id );
        endif;


        if ( $this->is_report ):
            $icon  = get_field( "report_global_icon", 'option' );
            $color = get_field( "report_global_color", 'option' );
        endif;

        if ( ! $icon )
            return '';


        $svg = file_get_contents( $icon['url'] );

        $html = "
            <div class='icon-container' style='background-color: {$color}'>
                {$svg}
            </div>
        ";

        return $html;


    }

    public function card_type() {

        if ( $this->is_report )
            return "Report Overview";

        $types = get_the_terms( $this->post, 'media_type' );

        if ( ! is_array( $types ) )
            return 'No Type';

        return $types[0]->name;

    }

    public function youtube_url() {

        if ( $this->post->post_type === 'cew_reports' )
            return false;
        if ( $this->media_type[0]->slug !== 'video' )
            return false;

        $link = $this->fields['post_link'];

        $yt_link      = strpos( $link, 'youtube' );
        $yt_shortlink = strpos( $link, 'youtu.be' );

        if ( ! $yt_link && ! $yt_shortlink )
            return false;

        return $link;


    }

    public function get_report() {


        global $wpdb;

        $reports = $wpdb->get_results( '
            select p.*
            from wp7610_posts p
            join wp7610_postmeta m on m.post_id = p.ID
            where post_type = "cew_reports"
            and post_status = "publish"
            and m.meta_key = "resources"
            and m.meta_value like "%\"' . $this->post->ID . '\"\;%"
            order by id
            
        ' );


        if ( ! count( $reports ) )
            return false;

        return $reports[0];


    }

}
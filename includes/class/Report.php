<?

namespace TSD;

class Report {

    private $post;
    private $fields;

    public function __construct( $post ) {

        $this->post   = $post;
        $this->fields = get_fields( $post->ID );

    }

    public function get_card_html( $template = 'default' ) {
        return Acme::get_file( "components/card/$template.php", [ 'post' => $this->post ] );
    }


    public function get_post() {
        return $this->post;
    }

    public function get( $field ) {

        if ( ! array_key_exists( $field, $this->fields ) )
            return null;

        return $this->fields[ $field ];

    }

    static public function resource_query() {

    }

    static public function get_resources( $id, $return_as_posts = false ) {
        global $wpdb;
        $sql_string = "select post_id
            from {$wpdb->postmeta}
            where meta_key = 'reports'
              and meta_value like \"%{$id}%\"
              and post_id in (select ID from {$wpdb->posts} where post_status = 'publish' and post_type = 'resource')
        " ;

        API::tsd_write_log($sql_string);
        $query = $wpdb->get_results( $sql_string );
        API::tsd_write_log($query);

        $ids = [$id];


        foreach ( $query as $r )
            $ids[] = $r->post_id;




        $q = new \WP_Query( [ 'post_type' => 'any', 'post__in' => $ids ] );

        $resources = [];
        foreach ( $q->posts as $post ):
            if ( $return_as_posts ) {
                $resources[] = $post;
            }
            else {
                $others      = new Card( $post );
                $resources[] = $others->get_template_html();
            }
        endforeach;

        return $resources;

    }
}
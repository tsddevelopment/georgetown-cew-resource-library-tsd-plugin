<?

namespace TSD;

class API {

    private $api_base = 'research';

    public function __construct() {

        $this->register_api_endpoint( 'get_posts', 'get_posts_fn', 'POST' );
        $this->register_api_endpoint( 'get_research_posts', 'get_research_posts_fn', 'POST' );

    }

    private function register_api_endpoint( $endpoint = '', $callback, $method = 'GET' ) {
        if ( $endpoint == '' )
            return;


        add_action( 'rest_api_init', function() use ( $endpoint, $callback, $method ) {
            register_rest_route( $this->api_base, $endpoint, [ 'methods'  => $method,
                                                               'callback' => [ $this, $callback ], ] );
        } );
    }


    public static function get_posts_fn() {

        // i'm pretty sure this is a massive security vulnerability lol
        $posts = Acme::get_library( $_POST );

        $return = [];

        foreach ( $posts as $post ):
            $report = new Card( $post );
            //            $return[] = $post->post_title;
            $return[] = $report->get_template_html();

        endforeach;

        return $return;

    }

    public static function get_research_posts_fn() {

        // tsd_write_log( $_POST );

        $page_no        = isset( $_POST['page_no'] ) && $_POST['page_no'] ? $_POST['page_no'] : 1;
        $resource_types = [];
        $orderby        = isset( $_POST['orderby'] ) && $_POST['orderby'] ? $_POST['orderby'] : 'date';
        $order          = isset( $_POST['order'] ) && $_POST['order'] ? $_POST['order'] : 'desc';

        $qry_args = [ 'paged'          => $page_no,
                      'posts_per_page' => 12,
                      'orderby'        => $orderby,
                      'order'          => $order ];

        $posts         = [];
        $max_num_pages = 1;
        $return        = [ 'max_num_pages' => $max_num_pages,
                           'data'          => [], ];

        if ( array_key_exists( 'publication_year', $_POST ) && $_POST['publication_year'] !== '' )
            $qry_args['date_query'] = [ [ 'year' => $_POST['publication_year'] ] ];

        $qry_args = \wp_parse_args( $qry_args, self::tsd_prepare_query_args() );

        $query = Acme::get_library( $qry_args, [ 'return_query' => true, 'use_swp_query' => true ] );

        if ( $query ) {
            $max_num_pages = $query->max_num_pages;

            $posts = $query->posts;
        }


        if ( $query->found_posts )
            foreach ( $posts as $post ):
                $card = new Card( $post );
                //            $return[] = $post->post_title;
                $return['data'][] = $card->get_template_html();

            endforeach;


        $return['query_args']    = $qry_args;
        $return['post']          = $_POST;
        $return['max_num_pages'] = $max_num_pages;

        if ( ! $return['max_num_pages'] && $query->found_posts ) {
            $return['max_num_pages'] = 1;
        }


        $return['found_posts'] = $query->found_posts;
        $return['card_rows']   = [ ceil( $query->found_posts / 2 ),
                                   ceil( $query->found_posts / 3 ), ];

        return $return;

    }


    public static function tsd_prepare_query_args( $data = [] ) {

        $page_no        = isset( $_POST['page_no'] ) && $_POST['page_no'] ? $_POST['page_no'] : 1;
        $resource_types = [];
        $qry_args       = [ 's'              => '',
                            'paged'          => $page_no,
                            'posts_per_page' => 12,
                            'orderby'        => 'date',
                            'order'          => 'DESC' ];


        $data = $data ? $data : $_POST;

        $tax_query          = [];
        $qry_args_from_data = [];


        foreach ( $data as $key => $value ) {

            if ( ! is_array( $value ) )
                $value = urldecode( $value );

            if ( ! $value )
                continue;


            switch ( $key ):

                case 'research_type':

                    $vars = [ 'taxonomy' => 'research_area',
                              'field'    => 'slug',
                              'terms'    => $value, ];

                    $tax_query['research_area'] = $vars;

                    break;


                case 'report_keyword':
                case 'keyword':
                    $qry_args_from_data['s'] = $value;
                    break;


                case 'media_type':

                    $vars = [ 'taxonomy' => 'media_type',
                              'field'    => 'slug',
                              'terms'    => $value, ];

                    $tax_query['media_type'] = $vars;
                    break;


                case 'report_tag':
                    $vars = [ 'taxonomy' => 'report_tag',
                              'field'    => 'slug',
                              'terms'    => $value, ];

                    $tax_query['report_tag'] = $vars;
                    break;

            endswitch;


        }

        $qry_args = wp_parse_args( $qry_args_from_data, $qry_args );

        if ( count( $tax_query ) ) {
            // i dont think this actually comes up
            $qry_args['tax_query'] = $tax_query;

            if ( count( $tax_query ) > 1 ) {
                $qry_args['tax_query']['relation'] = 'AND';
            }
        }


        return $qry_args;
    }

    public static function tsd_write_log( $msg ) {

        // no need for this right now :)
//        return;

        $file = dirname( __FILE__ ) . '/tsd_log.log';

        $date = date( 'm/d/Y h:i:s a' );

        if ( is_array( $msg ) || is_object( $msg ) ) {
            $msg = print_r( $msg, true );
        }

        $fn = debug_backtrace()[1]['function'];

        $msg = '[ ' . $date . ' : ' . $fn . ' ]: ' . $msg;
        $msg .= "\r\n\r\n";

        // Write the contents to the file,
        // using the FILE_APPEND flag to append the content to the end of the file
        // and the LOCK_EX flag to prevent anyone else writing to the file at the same time
        file_put_contents( $file, $msg, FILE_APPEND | LOCK_EX );
    }

}

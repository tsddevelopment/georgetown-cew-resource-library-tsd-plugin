<?

$active_research_type = false;

global $wp_query;
$qv = $wp_query->query_vars;

$resource_taxonomy = '';

if ( array_key_exists( 'research_area', $qv ) ) {
    $active_research_type = $qv['research_area'];
    $resource_taxonomy    = 'research_area';
}

else if ( array_key_exists( 'media_type', $qv ) ) {
    if ( $qv['media_type'] != 'webpage' ) {
        $active_research_type = $qv['media_type'];
        $resource_taxonomy    = 'media_type';
    }
}


$default_image    = get_field( "image", 16943 )['sizes']['large'];
$default_title    = get_field( "title", 16943 );
$default_content  = get_field( "content", 16943 );
$term_image       = '';
$term_name        = '';
$term_description = '';

if ( $active_research_type ) {
    if ( $research_area = get_term_by( 'slug', $active_research_type, $resource_taxonomy ) ) {
        $default_title = $research_area->name;
        if ( $research_area->description != '' )
            $default_content = $research_area->description;
        if ( $term_image = get_field( 'icon', $research_area ) ) {
            $term_image = $term_image['url'];
        }

        if ( $resource_taxonomy == 'media_type' )
            $term_color = get_field( 'color', $research_area );
    }
    if ( $term_image === '' )
        $term_image = false;
}

?>
<div class="vc_spacer"></div>
<div class="cew-resource-hero" data-default-img="<?= htmlentities( $default_image ); ?>" role="banner"
     aria-label="Resource Library Content Information">
    <div class="">
        <div class="cew-resource-hero-img">
            <? if ( $resource_taxonomy === 'media_type' ): ?>
                <? $icon_id = uniqid( 'term_svg_' ) ?>
                <div id="<?= $icon_id ?>" class="svg_container"
                     style="background: <?= $term_color ?>; margin-right: 40px;">
                    <?= file_get_contents( $term_image ); ?>
                </div>
            <? else: ?>
                <img src="<?= esc_url( ( $term_image ? $term_image : $default_image ) ) ?>"
                     alt="Browse Our Online Resource Library">
            <? endif ?>
        </div>
        <div class="content"
             role="banner"
             aria-label="CEW Resource Library"
             tabindex="0"
        >
            <h1 class="cew-resource-hero-heading" tabindex="0"><?= $default_title ?></h1>
            <hr style='border: 2px solid #f9a21c; width: 30%;margin-left:0;'>
            <p class='header' tabindex="0"><?= $default_content ?></p>
        </div>
    </div>
</div>

<?
$query_vars_from_url_parse = [ 'report_title',
                               'research_area',
                               'report_keyword',
                               'media_type' ];

$search_filters = [];

foreach ( $query_vars_from_url_parse as $key ):

    $value = urldecode( get_query_var( $key ) );

    if ( ! $value )
        continue;

    if ( $key == 'media_type' ) {
        $value = array_filter( array_map( 'trim', explode( ',', $value ) ) );
    }

    $search_filters[ $key ] = $value;

endforeach;

$home_page_posts = \TSD\Acme::get_library( [ 'posts_per_page' => 5, 'post_type' => 'cew_reports' , 'post_status' => 'publish']);
?>

<div class="cew-resource-query-container clearfix" role="form">
    <div class="cew-resource-query-header">
        <h2>Resources</h2>
    </div>
    <?= \TSD\Acme::library_query_form( [ 'type_filter' => false, 'search_filter' => $search_filters ] ) ?>
</div>

<div class="col">
    <div class="pt-5 cew-home-resource-container" role="main" aria-label="Resource Library Sample">
        <? foreach ( $home_page_posts as $c => $wp_obj ):
            $first    = $c === 0;
            $c        = new TSD\Card( $wp_obj );
            $template = $first ? 'featured' : 'default';
            print $c->get_template_html( $template );
         endforeach
        ?>
    </div>
</div>
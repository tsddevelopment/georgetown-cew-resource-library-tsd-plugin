<?
$banner = get_field( "banner" );
if ( ! $banner )
    return;
?>

<div class="cew-about-us py-5">
    <? if ( $banner[0] ): ?>
        <div class="element">
            <div class="text-container vc_col-sm-7">
                <?= $banner[0]['content'] ?>
            </div>
            <div class="img-container">
                <img src="<?= $banner[0]['image']['sizes']['large'] ?>" alt="">
            </div>
        </div>
    <? endif ?>
    <? if ( $banner[1] ): ?>
        <div class="element">
            <div class="img-container">
                <img src="<?= $banner[1]['image']['sizes']['large'] ?>" alt="">
            </div>
            <div class="text-container vc_col-sm-7">
                <?= $banner[1]['content'] ?>
            </div>
        </div>
    <? endif ?>
    <? if ( $banner[2] ): ?>
        <div class="element">
            <div class="text-container vc_col-sm-7">
                <?= $banner[2]['content'] ?>
            </div>
            <div class="img-container">
                <img src="<?= $banner[2]['image']['sizes']['large'] ?>" alt="">
            </div>
        </div>
    <? endif ?>
</div>

<?
$terms = \TSD\Acme::get_research_areas();
?>


<div class="py-5 cew-home-research-type-container" role="complementary">
    <!--    <h6 class="pb-4">reports/meta</h6>-->
    <? foreach ( $terms as $c => $term ):
        $iconURL = get_field( 'icon', $term->taxonomy . '_' . $term->term_id ); ?>
        <? $url = site_url( "resources/research/{$term->slug}" ); ?>

        <div class="vc_col-sm-12 cew-research-type" onclick="location.href='<?= $url ?>'" tabindex="0">
            <div class="image">
                <img src="<?= $iconURL['sizes']['large'] ?>" alt="<?= $term->name ?> Cover Image" />
            </div>
            <div class="boxshadow">
                <div class="box-content">
                    <h4><?= $term->name ?></h4>
                    <?= wpautop( $term->description ) ?>
                </div>
            </div>
        </div>

    <? endforeach ?>
</div>

<?php

/**
 * this file is going to power an ajax enabled search call
 *
 * 0 - the content will load with
 * 1 - the user will submit a query through the search
 * 2 - the query will hit the api
 * 3 - the api will return formatted report/meta results
 * 4 - the formatted content will replace the existing content
 *
 * then! we need to paginate the results with an async "infinite
 * scroll" loading.
 *
 */

?>

<div class="pt-3 col">
    <?

    $query_vars_from_url_parse = [ 'report_title',
                                   'research_area',
                                   'report_keyword',
                                   'media_type' ];

    $search_filters = [];

    foreach ( $query_vars_from_url_parse as $key ):

        $value = urldecode( get_query_var( $key ) );

        if ( ! $value )
            continue;

        if ( $key == 'media_type' ) {
            $value = array_filter( array_map( 'trim', explode( ',', $value ) ) );
        }

        $search_filters[ $key ] = $value;

    endforeach;

    $qry_args = \TSD\API::tsd_prepare_query_args( $search_filters );
    $qry      = \TSD\Acme::get_library( $qry_args, [ 'use_swp_query' => true ] );
    ?>

    <div class="cew-resource-query-container clearfix" role="complementary" aria-label="Resource Library Current Display Count">
        <div class="cew-resource-query-header">
            <h2 aria-label="Total Resources Found Label">Resources</h2>
        </div>
        <?= \TSD\Acme::library_query_form( [ 'type_filter' => true, 'search_filter' => $search_filters ] ) ?>
        <div class="cew-result-counter hidden">
            <h5 class="count" aria-label="Total Resources Currently Displayed">0</h5>
            <p>Resources</p>
            <? // this is the path that forms the circular progress bar ?>
            <svg viewBox="0 0 36 36" preserveAspectRatio="xMinYMin meet" class="circular-chart">
                <path class="circle bg" stroke-dasharray="100, 100"
                      d="M18 2.0845
                  a 15.9155 15.9155 0 0 1 0 31.831
                  a 15.9155 15.9155 0 0 1 0 -31.831"
                />
                <path class="circle progress" stroke-dasharray="0, 100"
                      d="M18 2.0845
                  a 15.9155 15.9155 0 0 1 0 31.831
                  a 15.9155 15.9155 0 0 1 0 -31.831"
                />
            </svg>
        </div>
        <div class="cew-scroll-to-top hidden">
            <p>Scroll<br />to top</p>
        </div>
    </div>
</div>

<div class="spinner hidden">
    <div class="card"></div>
    <div class="card"></div>
    <div class="card"></div>
</div>

<div class="pt-5 col">

    <div class="cew-resource-library-container cew-content clearfix" role="main" aria-label="CEW Resource Library Asset Display">
        <? foreach ( $qry as $c => $wp_obj ): ?>
            <? $c = new \TSD\Card( $wp_obj ); ?>
            <?= $c->get_template_html(); ?>
        <? endforeach ?>

    </div>

    <div class="load-more text-center clear hidden">
        <div class="card"></div>
        <div class="card"></div>
        <div class="card"></div>
    </div>
</div>


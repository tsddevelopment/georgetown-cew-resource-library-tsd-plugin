<?

$resources = get_field( "resources" );

if ( ! $resources )
    return false;

$dimension_lookup = [ 'half' => [ 'width' => '1/2',
                                  'chunk' => 2 ],
                      'full' => [ 'width' => '1/6',
                                  'chunk' => 6 ] ];

$ppr = is_array( $atts ) ? $atts['ppr'] : 4;

$chunk = $ppr;
$width = "1/$ppr";

$resources = array_chunk( $resources, $chunk );

$rows = '';

foreach ( $resources as $row ):

    $cols = '';

    foreach ( $row as $post ):

        ob_start();
        $terms = get_the_terms( $post, 'media_type' );
        include( PLUGIN_DIR . '/public/resource_listing/tile_template.php' );
        $html = ob_get_clean();

        $cols .= "    
            [vc_column_inner width='$width' ]
                $html
            [/vc_column_inner]
            ";
    endforeach;

    $rows .= "[vc_row_inner equal_height='yes']{$cols}[/vc_row_inner]";

endforeach;


print do_shortcode( $rows );
<?


$post  = $card->get_post();
$types = [ 'research_area', 'media_type' ];

// cew_report post types don't have resource_types, because in this
// project, their type is always considered "full report" - which
// i haven't figured out how i want to program yet.


$pub_date   = date( "F j, Y", strtotime( $post->post_date ) );
$reports    = $card->get_field( "reports" );
$post_link  = $card->get_field( "post_link" );
$embed_link = $card->get_field( "embed_url" );


$link_report_id = false;
if ( $reports && count( $reports ) )
    $link_report_id = $reports[0]->ID;


$media_type = get_the_terms( $post, "media_type" );

$media_type = is_array( $media_type ) ? $media_type[0]->slug : '';

//if ($post->post_type === 'cew_reports')
//    $resources = \TSD\Report::get_resources($post->ID);

$areas = get_the_terms( $post, 'research_area' );

$research_area      = is_array( $areas ) ? $areas[0]->name : '';
$research_area_slug = is_array( $areas ) ? $areas[0]->slug : '';

$report_link = '';

$embed_url = $embed_link ? $embed_link : $post_link;

if ( $card->youtube_url() ):
    $embed_url  = $card->youtube_url();
    $media_type = 'yt_embed';
endif;

if ( $post->post_type === 'cew_reports' ):
    $media_type = 'cew_reports';
    $embed_url  = get_permalink( $post->ID );
endif;
$url = $card->get_img();
$lrg_url = $card->get_img('large');


?>

<div
        class="cew-resource-card featured"
        onclick="cew_card_click_fn()"
        data-id="<?= $post->ID ?>"
        data-card-embed-url="<?= $embed_url ?>"
        data-card-media-type="<?= $media_type ?>"
        data-card-post-type="<?= $post->post_type ?>"
        data-card-research-area="<?= $research_area_slug ?>"
        tabindex="0"
>
    <div class="image"
         style="background-image: url(<?= $lrg_url ?>);">
        <img src="<?= $url ?>" alt="<?= $post->post_title ?> Cover Image" />
    </div>


    <div class="cew-icon-holder">
        <?= $card->resource_icon() ?>
    </div>

    <div class="content">


        <? if ( $card->card_type() ): ?>
            <h5 class="type"><?= $card->card_type() ?></h5>
        <? endif ?>
        <? if ( $pub_date ): ?>
            <h6 class="publication_date"><?= $pub_date ?></h6>
        <? endif ?>
        <? if ( $research_area ): ?>
            <h6 class="research-area"><?= $research_area ?></h6>
        <? endif ?>


        <? $card_title = explode( ":", $post->post_title ) ?>
        <h3 class="title"><?= $card_title[0] ?>
            <? if ( array_key_exists( 1, $card_title ) ): ?>
                <br>
                <span class="subtitle"><?= $card_title[1] ?></span>
            <? endif ?>
        </h3>
    </div>
</div>


<?

$query = new \WP_Query(['post_type'      => 'cew_reports',
                        'posts_per_page' => 3]);

foreach ($query->posts as $post):
    $report = new \TSD\Report($post);
    print $post->post_title;
    print $report->get_card_html('card-excerpt');
endforeach;
var gulp = require('gulp');
var $    = require('gulp-load-plugins')();

var sassPaths = [
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];

gulp.task('sass', function() {
  return gulp.src([ 'public/scss/*.scss', "!public/scss/_*.scss" ])
      .pipe($.sourcemaps.init())
      .pipe($.sass({
        includePaths: sassPaths,
        outputStyle: 'compressed' //options; expanded, nested, compact, compressed
      })
      .on('error', $.sass.logError))
      .pipe($.autoprefixer({
        browsers: ['last 10 versions', 'ie >= 10', 'ios 6']
      }))
      .pipe($.sourcemaps.write())
      .pipe(gulp.dest('public/css'));
});

gulp.task('default', ['sass'], function() {
  gulp.watch(['public/scss/**/*.scss'], ['sass']);
});
<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://topshelfdesign.net
 * @since             1.0.0
 * @package           Gcew_Resource_Library
 *
 * @wordpress-plugin
 * Plugin Name:       Georgetown CEW Resource Library
 * Plugin URI:        https://cew.georgetown.edu
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Top Shelf Design
 * Author URI:        https://topshelfdesign.net
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gcew-resource-library
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

add_action( "init", function() {
    define( "TSD_USER", get_current_user_id() === 25 );
} );

define("CEW_PLUGIN_DIR", plugin_dir_path( __FILE__ ));

require_once plugin_dir_path( __FILE__ ) . 'kint.phar';
require_once plugin_dir_path( __FILE__ ) . 'includes/class/Acme.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/class/Resource.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/class/Report.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/class/Card.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/class/API.php';


function gcew_add_media_type_to_resource_relationship( $title, $post, $field, $post_id ) {

    $terms = get_the_terms( $post, 'media_type' );

    if ( ! count( $terms ) )
        return $title;

    return "$title | {$terms[0]->name}";
}

function gcew_filter_report_resource_relationship_field( $args, $field, $post_id ) {

    $args['tax_query'] = [ [ 'taxonomy' => 'media_type',
                             'operator' => 'IN',
                             'terms'    => get_terms( "media_type", [ 'fields' => 'ids' ] ) ] ];

    return $args;
}


// filter for every field
add_filter( 'acf/fields/relationship/result/key=field_5e17905a043d8', 'gcew_add_media_type_to_resource_relationship', 10, 4 );
add_filter( 'acf/fields/relationship/query/key=field_5e17905a043d8', 'gcew_filter_report_resource_relationship_field', 10, 4 );

if ( array_key_exists( "report", $_GET ) ):

    add_action( "wp", function() {

        $atts = [ 'post_type'      => 'resource',
                  'posts_per_page' => - 1,
                  'tax_query'      => [ [ 'taxonomy' => 'media_type',
                                          'field'    => 'slug',
                                          'terms'    => 'newsletter' ] ] ];

        $q = new WP_Query( $atts );

        foreach ( $q->posts as $post )
            set_post_thumbnail( $post->ID, 18238 );


        print 'success';


        die();
    } );


endif;


if ( array_key_exists( "test", $_GET ) ):

    add_action( 'wp', function() {

        $q = $_GET['test'];

        $atts = [ 's'         => $q,
                  'post_type' => [ 'cew_reports', 'resource' ] ];
        $opts = [ 'return_query' => true ];

        $query = TSD\Acme::get_library( $atts, $opts );

        Kint::dump( $query );

        print "<h4>$query->found_posts found for query $q</h4> ";



        if ( ! $query->found_posts )
            print 'no posts found';
        else
            foreach ( $query->posts as $post )
                print wpautop( $post->post_title . ' ' . $post->post_type );

        die();
    } );


endif;


new \TSD\API();


/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'GCEW_RESOURCE_LIBRARY_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gcew-resource-library-activator.php
 */
function activate_gcew_resource_library() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-gcew-resource-library-activator.php';
    Gcew_Resource_Library_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gcew-resource-library-deactivator.php
 */
function deactivate_gcew_resource_library() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-gcew-resource-library-deactivator.php';
    Gcew_Resource_Library_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gcew_resource_library' );
register_deactivation_hook( __FILE__, 'deactivate_gcew_resource_library' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gcew-resource-library.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gcew_resource_library() {

    $plugin = new Gcew_Resource_Library();

    function cc_mime_types( $mimes ) {
        $mimes['svg'] = 'image/svg+xml';

        return $mimes;
    }

    add_filter( 'upload_mimes', 'cc_mime_types' );

    $plugin->run();

    if ( function_exists( 'acf_add_local_field_group' ) ):

        $global_sub_fields = [ [ 'return_format' => 'array',
                                 'preview_size'  => 'medium',
                                 'library'       => 'all',
                                 'key'           => 'field_5e1f6045365e7',
                                 'label'         => 'Image',
                                 'name'          => 'image',
                                 'type'          => 'image',
                                 'instructions'  => '',
                                 'required'      => 1,
                                 'wrapper'       => [ 'width' => '30',
                                                      'class' => '',
                                                      'id'    => '', ], ],
                               [ 'tabs'          => 'all',
                                 'toolbar'       => 'full',
                                 'media_upload'  => 1,
                                 'default_value' => '',
                                 'delay'         => 0,
                                 'key'           => 'field_5e1f6051365e8',
                                 'label'         => 'Content',
                                 'name'          => 'content',
                                 'type'          => 'wysiwyg',
                                 'instructions'  => '',
                                 'required'      => 1,
                                 'wrapper'       => [ 'width' => '70',
                                                      'class' => '',
                                                      'id'    => '', ], ], ];

        acf_add_local_field_group( [ 'key'                   => 'group_5e1f6035d3a77',
                                     'title'                 => 'Banner Options',
                                     'fields'                => [ [ 'sub_fields'        => $global_sub_fields,
                                                                    'min'               => 0,
                                                                    'max'               => 3,
                                                                    'layout'            => 'block',
                                                                    'button_label'      => '',
                                                                    'collapsed'         => '',
                                                                    'key'               => 'field_5e1f603e365e6',
                                                                    'label'             => 'Banner',
                                                                    'name'              => 'banner',
                                                                    'type'              => 'repeater',
                                                                    'instructions'      => '',
                                                                    'required'          => 0,
                                                                    'conditional_logic' => 0,
                                                                    'wrapper'           => [ 'width' => '',
                                                                                             'class' => '',
                                                                                             'id'    => '', ], ], ],
                                     'location'              => [ [ [ 'param'    => 'page',
                                                                      'operator' => '==',
                                                                      'value'    => '16926', ], ], ],
                                     'menu_order'            => 0,
                                     'position'              => 'normal',
                                     'style'                 => 'default',
                                     'label_placement'       => 'top',
                                     'instruction_placement' => 'label',
                                     'hide_on_screen'        => '',
                                     'active'                => 1,
                                     'description'           => '', ] );


        acf_add_local_field_group( [ 'key'                   => 'group_5e1f651a5b5bf',
                                     'title'                 => 'Resource Banner',
                                     'fields'                => [ [ 'tabs'              => 'all',
                                                                    'toolbar'           => 'full',
                                                                    'media_upload'      => 1,
                                                                    'default_value'     => '',
                                                                    'delay'             => 0,
                                                                    'key'               => 'field_5e1f652a2056f',
                                                                    'label'             => 'Title',
                                                                    'name'              => 'title',
                                                                    'type'              => 'text',
                                                                    'instructions'      => '',
                                                                    'required'          => 0,
                                                                    'conditional_logic' => 0,
                                                                    'wrapper'           => [ 'width' => '',
                                                                                             'class' => '',
                                                                                             'id'    => '', ], ],
                                                                  [ 'return_format'     => 'array',
                                                                    'preview_size'      => 'medium',
                                                                    'library'           => 'all',
                                                                    'min_width'         => '',
                                                                    'min_height'        => '',
                                                                    'min_size'          => '',
                                                                    'max_width'         => '',
                                                                    'max_height'        => '',
                                                                    'max_size'          => '',
                                                                    'mime_types'        => '',
                                                                    'key'               => 'field_5e1f65222056d',
                                                                    'label'             => 'Image',
                                                                    'name'              => 'image',
                                                                    'type'              => 'image',
                                                                    'instructions'      => '',
                                                                    'required'          => 0,
                                                                    'conditional_logic' => 0,
                                                                    'wrapper'           => [ 'width' => '30',
                                                                                             'class' => '',
                                                                                             'id'    => '', ], ],
                                                                  [ 'tabs'              => 'all',
                                                                    'toolbar'           => 'full',
                                                                    'media_upload'      => 1,
                                                                    'default_value'     => '',
                                                                    'delay'             => 0,
                                                                    'key'               => 'field_5e1f652a2056e',
                                                                    'label'             => 'Content',
                                                                    'name'              => 'content',
                                                                    'type'              => 'wysiwyg',
                                                                    'instructions'      => '',
                                                                    'required'          => 0,
                                                                    'conditional_logic' => 0,
                                                                    'wrapper'           => [ 'width' => '70',
                                                                                             'class' => '',
                                                                                             'id'    => '', ], ], ],
                                     'location'              => [ [ [ 'param'    => 'page',
                                                                      'operator' => '==',
                                                                      'value'    => '16943', ], ], ],
                                     'menu_order'            => 0,
                                     'position'              => 'normal',
                                     'style'                 => 'default',
                                     'label_placement'       => 'top',
                                     'instruction_placement' => 'label',
                                     'hide_on_screen'        => '',
                                     'active'                => 1,
                                     'description'           => '', ] );


    endif;


}

run_gcew_resource_library();
